<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Subtype extends Model
{
    use Translatable;
    protected $table = 'subtypes';
    protected $fillable = [
        'id','name','icon','type_id','created_at','updated_at'
    ];
    protected $translatable  = ['name'];

    public function getSubtypeById($id,$lang){
        $objSubtype = $this->where('id',$id)->get();
        $objSubtypeTrans = $objSubtype->translate('en',$lang);
        $objSubtype2 = translationHelper::translatedCollectionToArray($objSubtypeTrans);
        
        return $objSubtype2;
    }
    public function getSubTypeAttachedToType($type_id,$lang){
        $arrSubType = $this->where('type_id',$type_id)->get();
        $arrSubTypeTrans = $arrSubType->translate('en',$lang);
        $arrSubType2 = translationHelper::translatedCollectionToArray($arrSubTypeTrans);
        return $arrSubType2;
    }
}

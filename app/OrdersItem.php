<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class OrdersItem extends Model
{
    use Translatable;
    protected $table = 'ordersItems';
    protected $fillable = [
        'id','product_id','user_id','order_id','created_at','updated_at'
    ];

    public function getOrderItemAttachedToOrder($order_id,$lang){
        $arrOrder = $this->where('order_id',$order_id)->get();
        $arrOrderTrans = $arrOrder->translate($lang,'en');
        $arrOrder2 = translationHelper::translatedCollectionToArray($arrOrderTrans);
        return $arrOrder2;

    }
}

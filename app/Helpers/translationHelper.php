<?php

namespace App\Helpers;


use TCG\Voyager\Traits\Translatable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class translationHelper
{

   static function paginationForTranslator($arrBeforeTranslate,$arrAfterTranslate){

        $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
        $arrAfterTranslate,
        $arrBeforeTranslate->total(),
        $arrBeforeTranslate->perPage(),
        $arrBeforeTranslate->currentPage(), [
            'path' => \Request::url(),
            'query' => [
                'page' => $arrBeforeTranslate->currentPage()
            ]
        ]
      );

       return $itemsTransformedAndPaginated;

   }
   static function translatedCollectionToArray(\TCG\Voyager\Translator\Collection $translatedCollection)
    {
    	//dd($translatedCollection);
	    $collectionArray = array();
	    foreach($translatedCollection as $collectionElement)
	    {

	        $collectionArray[] = array_map(function($rawAttributeData) { return $rawAttributeData['value']; },
	        	$collectionElement->getRawAttributes());
	    }
	    return $collectionArray;
	}



}

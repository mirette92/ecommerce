<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Respond;

class TypeController extends Controller
{
    //
    public function ListType(){
        $arr = array();
        $objType = new Type();
        $arrType = $objType->getTypesAttachedToSubtype();
        $arr['data'] = $arrType;
        $arr = Respond::mergeStatus($arr,200);
        return $arr;

    }
}

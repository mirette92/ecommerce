<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //

    public function Register(Request $request){
        
        $objUser = new User();
        // $objUser->name = $request->name;
        // $objUser->email = $request->email;
        // $objUser->password = Bcrypt($request->password) ;
        // $result = $objUser->save();
        // $proxy = Request::create(
        //     'oauth/token',
        //     'POST'
        // );
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
 
        $token = $user->createToken('token')->accessToken;
        return response()->json(['token' => $token], 200);
        return $result;
    }


    public function Login(Request $request){
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('token')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

}

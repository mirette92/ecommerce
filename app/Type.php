<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Type extends Model
{
    use Translatable;
    protected $table = 'types';
    protected $fillable = [
        'id','name','icon','created_at','updated_at'
    ];
    protected $translatable  = ['name'];

    public function listType($lang){
        $arrType = $this->get()->translate($lang,'en');
        $arrTypeTrans = $arrType->translate($lang,'en');
        $arrType2 = translationHelper::translatedCollectionToArray($arrTypeTrans);
        return $arrType2;
    }
    public function getTypesAttachedToSubtype($lang){
        $arrType = $this->get()->translate($lang,'en');
        $arrTypeTrans = $arrType->translate($lang,'en');
        foreach($arrTypeTrans as $objType){
            $objSubtype = new Subtype();
            $objType['Subtype'] = $objSubtype->getSubTypeAttachedToType($objType['id'],$lang);
        }
        $arrType2 = translationHelper::translatedCollectionToArray($arrTypeTrans);
        return $arrType2;
    }
}

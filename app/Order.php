<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Order extends Model
{
    use Translatable;
    protected $table = 'orders';
    protected $fillable = [
        'id','user_id','order_code','created_at','updated_at'
    ];
    
    public function getOrderAttachedToUser($user_id,$lang){
        $arrOrder = $this->where('user_id',$user_id)->get();
        $arrOrderTrans = $arrOrder->translate('en',$lang);
        foreach($arrOrderTrans as $obj){
            $objOrderItem = new OrderItem();
            $obj['sybtype'] = $objOrderItem->getOrderItemAttachedToOrder($obj->id,$lang);
        }
        $arrOrder2 = translationHelper::translatedCollectionToArray($arrOrderTrans);
        
        return $arrOrder2;
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use App\Subtype;
class Product extends Model
{
    use Translatable;
    protected $table = 'products';
    protected $fillable = [
        'id','name','feature_img','multi_img','short_desc','long_desc','price','subtype_id','created_at','updated_at'
    ];
    protected $translatable  = ['name','short_desc','long_desc'];

    public function list($Subtype_id,$lang){
        $arrProduct = $this->where('subtype_id',$Subtype_id)->get();
        $arrProductTrans = $arrProduct->translate('en',$lang);
        foreach($arrProductTrans as $obj){
            $objSubType = new Subtype();
            $obj['sybtype'] = $objSubType->getSubtypeById($obj->subtype_id,$lang);
        }
        $arrProduct2= translationHelper::translatedCollectionToArray($arrProductTrans);
        
        return $arrProduct2;
    }
}
